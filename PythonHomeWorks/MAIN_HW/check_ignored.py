import os
import argparse
import re


def gitignore_find(project_dir):
    gitignore_path = os.path.join(project_dir, ".gitignore")
    if os.path.exists(gitignore_path):
        return gitignore_path
    return None


def gitignore_read(gitignore_path):
    ignored_files = []
    with open(gitignore_path, 'r') as f:
        for line in f:
            line = line.strip()
            if line and not line.startswith("#"):  # теперь можно писать пустые строки и комментарии в .gitignore
                ignored_files.append(line)
    return ignored_files


def matches_pattern(file_path, pattern):
    regex_pattern = re.escape(pattern).replace("\\*", ".*")
    return re.match(regex_pattern + "$", file_path) is not None


def check_ignored(project_dir):
    gitignore_path = gitignore_find(project_dir)
    if gitignore_path is None:
        print(".gitignore not found in the project directory.")
        return []

    ignored_files = gitignore_read(gitignore_path)
    ignored_files_paths = []
    for root, _, files in os.walk(project_dir):
        for file in files:
            file_path = os.path.relpath(os.path.join(str(root), str(file)),
                                        str(project_dir))  # IDE ругается, если насильно не конвертировать все в строки
            for pattern in ignored_files:
                if matches_pattern(file_path, pattern):
                    ignored_files_paths.append((file_path, pattern))
                    break
    '''
    print("Debug Info:")
    print(f"Found .gitignore at: {gitignore_path}")
    print(f"Ignored files patterns: {ignored_files}")
    print(f"Ignored files found: {ignored_files_paths}")
    было нужно для отладки, вдруг еще пригодится
    '''

    return ignored_files_paths


def main():
    parser = argparse.ArgumentParser(description="Check ignored files in a project directory using .gitignore")
    parser.add_argument("--project_dir", type=str, help="Path to the project directory", required=True)
    args = parser.parse_args()
    ignored_files_paths = check_ignored(args.project_dir)
    if ignored_files_paths:
        print("Ignored files:")
        for file_path, pattern in ignored_files_paths:
            print(f"{file_path} ignored by expression {pattern}")
    else:
        print("No files ignored by .gitignore found in the project directory.")


if __name__ == "__main__":
    main()

