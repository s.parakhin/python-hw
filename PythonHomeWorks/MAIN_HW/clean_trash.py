import os
import time
import argparse

def clean_trash(trash_folder_path, age_thr):
    script_dir = os.path.dirname(os.path.abspath(__file__))
    log_file_path = os.path.join(script_dir, "clean_trash.log")
    while True:
        current_time = time.time()
        deleted_items = []
        for root, dirs, files in os.walk(trash_folder_path, topdown=False):
            for file in files:
                file_path = os.path.join(root, file)
                file_age = current_time - os.path.getmtime(file_path)
                if file_age > age_thr:
                    os.remove(file_path)
                    deleted_items.append(file_path)
            for dir in dirs:
                dir_path = os.path.join(root, dir)
                if not os.listdir(dir_path):
                    os.rmdir(dir_path)
                    deleted_items.append(dir_path)
        with open(log_file_path, "a") as log_file:
            for item in deleted_items:
                log_file.write(item + "\n")
        time.sleep(1)

def main():
    parser = argparse.ArgumentParser(description="Clean trash folder")
    parser.add_argument("--trash_folder_path", type=str, help="Path to trash folder")
    parser.add_argument("--age_thr", type=int, help="Threshold age in seconds")
    args = parser.parse_args()

    if not args.trash_folder_path or not args.age_thr:
        print("Error: trash_folder_path and age_thr are required arguments.")
        return
    clean_trash(args.trash_folder_path, args.age_thr)


if __name__ == "__main__":
    main()
